import './App.css';
import Header from './components/Header';
import Forms from './components/Forms';
import Table from './components/Table';
import GreenTestProvider from './context/GreenTestProvider';

const App = () => {
  return (
    <GreenTestProvider>
      <Header />
      <Forms />
      <Table />
    </GreenTestProvider>
  )
}

export default App;

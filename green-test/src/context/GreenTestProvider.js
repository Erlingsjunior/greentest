import React, { useState } from 'react';
import PropTypes from 'prop-types';
import MyContext from './MyContext';

function GreenTestProvider ({ children }) {
  /* Criando um estado inicial*/
  const [form, setForm] = useState({
    cadastros: [],
  });

  const [edit, setEdit] = useState({
    NomeCompleto: "",
    DataDeNascimento: "",
    Estado: "",
    Cidade: "",
    Idade: "",
  });

  const [aux, setAux] = useState({
    NomeCompleto: "",
    DataDeNascimento: "",
    Estado: "",
    Cidade: "",
    Idade: "",
  });

  return (
    /* utilizando MyContext para encapsular tudo no provider*/
    <MyContext.Provider
      value={ {
        form,
        setForm,
        edit,
        setEdit,
        aux,
        setAux
      } }
    >
      { children }
    </MyContext.Provider>
  );
}

GreenTestProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GreenTestProvider;


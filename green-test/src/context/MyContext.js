import { createContext } from 'react';

const MyContext = createContext();

export default MyContext;

/* Projeto utilizando context e hooks caso necessário
o context torna mais dinamica a passagem das props e possibilida
a criação de estados compartilhados mais facilmente */
import React, { useContext } from 'react';
import MyContext from '../context/MyContext';
import trash from '../icons/trash-bin.svg';
import edit from '../icons/edit.svg';

/* import './Table.css'; */
import 'bootstrap/dist/css/bootstrap.min.css';


function Table() {
  const { form, setForm, setEdit, aux } = useContext(MyContext);


  const editCadastro = (cadastro, index) => {
    if ( aux.NomeCompleto !== "") {
      alert("Formulário não esta vazio. Edição não permitida no momento.")
      return
    }
    setEdit(cadastro)
    deleteCadastroForm(index)
  }



  const deleteCadastroForm = (index) => {
    form.cadastros.splice(index, 1);
    setForm({ ...form, cadastros: [...form.cadastros] })
  };


  return (
    <div className="container border border">
      <div>
        <table
          className="table table-bordered">
          <thead>
            <tr
              className="table-success"
            >
              <th
                scope="col"
                width="285em"
              >
                Nome Completo
              </th>

              <th
                scope="col"
                width="150em"
              >
                Data
              </th>

              <th
                scope="col"
                width="50em"
              >
                Idade
              </th>

              <th
                scope="col"
                width="185"
              >
                Estado
              </th>

              <th
                scope="col"
                width="190em"
              >
                Cidade
              </th>

              <th
                scope="col"
                width="110em"
              >
                Editar
              </th>

              <th scope="col">Excluir</th>
            </tr>
          </thead>
        </table>
      </div>

      <div>
        <table
          className="table table-bordered">
          {
            form.cadastros
              .map((cadastro, index) => (
                <tr
                  className="table table-bordered"
                >
                  <th scope="row"></th>
                  <td
                    width="245em"
                    Key={`${cadastro.NomeCompleto}`}
                    value={`${cadastro.NomeCompleto}`}
                  >
                    {cadastro.NomeCompleto}
                  </td>

                  <td
                    width="145em"
                    value={`${cadastro.DataDeNascimento}`}
                  >
                    {cadastro.DataDeNascimento}
                  </td>

                  <td
                    width="75em"
                    value={`${cadastro.Idade}`}
                  >
                    {cadastro.Idade}
                  </td>

                  <td
                    width="185"
                    value={`${cadastro.Estado}`}
                  >
                    {cadastro.Estado}
                  </td>

                  <td
                    width="190em"
                    value={`${cadastro.Cidade}`}
                  >
                    {cadastro.Cidade}

                  </td>

                  <td>
                    {<input
                      type="image"
                      src={edit}
                      alt="botao de editar"
                      width="50em"
                      height="30em"
                      onClick={() => editCadastro(cadastro, index)}
                    />}
                  </td>

                  <td>
                    {<input
                      type="image"
                      src={trash}
                      alt="botao de deletar"
                      width="55em"
                      height="40em"
                      onClick={() => deleteCadastroForm(index)}
                    />}
                  </td>
                </tr>
              ))
          }
        </table >
      </div>
    </div>

  )
}

export default Table;

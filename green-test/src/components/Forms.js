import React, { useState, useContext, useEffect } from 'react';
import MyContext from '../context/MyContext';
import PropTypes from 'prop-types';
import Lugares from './Lugares';

import './Forms.css';
import 'bootstrap/dist/css/bootstrap.min.css'

function Forms() {
  const { form, setForm, edit, setEdit, aux, setAux } = useContext(MyContext);
  
  /* const [aux, setAux] = useState({
    NomeCompleto: "",
    DataDeNascimento: "",
    Estado: "",
    Cidade: "",
    Idade: "",
  }); */

  useEffect(() => {
    if (aux.NomeCompleto !== "") {
      return;
    }
    
    if (edit.NomeCompleto !== "") {
      setAux(edit)
    }

  }, [aux.NomeCompleto, edit])

  let cidades = [
    "Selecione a Cidade"
  ];

  const selectEstados = () => (
    <select
      className="custom-select"
      id="inputGroupSelect01"
      /* id="estado" */
      data-testid="estado-filter"
      value={aux.Estado}
      name="estado"
      onChange={(event) => {
        setAux({ ...aux, Estado: (event.target.value) });
      }}
    >
      {Lugares
        .map((estado) => (
          <option key={estado} value={`${estado.nome}`}>
            {estado.nome}
          </option>
        ))}
    </select>
  );

  const selectCidades = () => (
    <select
      className="custom-select"
      id="inputGroupSelect01"
      value={aux.Cidade}
      data-testid="cidade-filter"
      name="cidade"
      onChange={(event) => {
        setAux({ ...aux, Cidade: (event.target.value) });
      }}
    >
      {Lugares.filter((estado) => {
        if (estado.nome === aux.Estado) {
          for (let i = 0; i <= estado.cidades.length; i++) {
            /* console.log(estado.cidades[i]) */
            cidades.push(estado.cidades[i])
            if (estado.nome === "Distrito Federal") {
              break;
            }
          }
        }
      })}

      {cidades
        .map((cidade) => (
          <option key={cidade} value={`${cidade}`}>
            {cidade}
          </option>
        ))}
    </select>
  );

  aux.Idade = new Date().getFullYear() - parseInt(aux.DataDeNascimento.split('-')[0]);

  const submitAuxForms = () => {
    if(!aux.NomeCompleto || !aux.DataDeNascimento || !aux.Estado || !aux.Cidade) {
      alert("O formulário deve estar completo para adicionar")
      return
    }
    setForm({ ...form, cadastros: [...form.cadastros, aux] })
    resetForms();
  };

  const resetForms = () => {
    const newData = {
      NomeCompleto: "",
      DataDeNascimento: "",
      Estado: "",
      Cidade: "",
      Idade: "",
    }
    setAux({ ...newData })
    setEdit({ ...newData })
  };

  return (
    <div className="container border border-success ">
      <div
        className="form-group"
      >
        <div
          className="input-group mb-3 d-block">
          <label
            className="input-group-text"
            for="inputGroupSelect01"
            htmlFor="name"
          >
            Nome Completo &nbsp;
        <input
              className="input-group"
              type="text"
              id="name"
              value={aux.NomeCompleto}
              data-testid="name-test"
              placeholder="Digite seu nome completo"
              onChange={(event) => setAux({ ...aux, NomeCompleto: event.target.value })}
            />
          </label>
        </div>

        <div
          className="input-group mb-3 d-block">
          <label
            class="input-group-text"
            for="inputGroupSelect01"
            htmlFor="date"
          >
            Data Nascimento &nbsp;
        <input
              className="input-group d-block"
              type="date"
              id="date"
              value={aux.DataDeNascimento}
              data-testid="date-test"
              onChange={(event) => setAux({ ...aux, DataDeNascimento: event.target.value })}
            />
          </label>
        </div>

        <div
          className="input-group mb-3 d-block">
          <label
            class="input-group-text"
            for="inputGroupSelect01"
            htmlFor="states"
          >
            Estado &nbsp;
        {selectEstados()}
          </label>
        </div>

        <div
          className="input-group mb-3 d-block">
          <label
            class="input-group-text"
            for="inputGroupSelect01"
            htmlFor="cidades"
          >
            Cidades &nbsp;
        {selectCidades()}
          </label>

          <button
            className="btn btn-outline-success"
            type="button"
            onClick={() => submitAuxForms()}
          >
            Adicionar
          </button>
        </div>

      </div>
    </div>
  )
}

Forms.propTypes = {
  filter: PropTypes.func,
};

export default Forms;

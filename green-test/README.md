# Projeto GreenTest

Projeto de avaliação, para teste de conhecimentos especificos, aplicação, cleanCode e raciocinio lógico.
Stacks utilizadas:
JavaScript,
React.Js ,
Context,
Hooks,
Jsx,
CSS.

Interface, Tabelas, Interação de dados, captura de dados em state local e state context (global)
Tarefas avançadas de CRUD.




## Sistema de Commits

Os commits foram feitos informando o processo atual que estava sendo realizado.
Para esse projeto especificamente, optei por nao deletar os commits apos o merge para nao perdermos a trilha
e ser possivel analisar a linha de raciocinio da construção do projeto.

### Considerações Finais

O projeto de CRUD em uma criação de página de cadastro, aparentemente simples mas com necessidade da criação de soluções
bem complexas para seu funcionamento, um projeto muito bom!

#### Da Busca de Estados/Cidades

Optei por criar uma component com um array de objetos incluindo todos os estados e cidades, nao foi minha primeira opção
à principio havia pensando em fazer uma requisição para API do IBGE, e manipular os dados recebidos e apresenta-los; mas depois 
de ponderar sobre a velocidade e confiabilidade do sistema, optei por criar um component onde podemos utilizar os dados e 
manipular de acordo com nossas necessidades, sem correr o risco de uma API cair e ficarmos fora do ar, a opção também é
válida para melhorar a performance da pesquisa.
